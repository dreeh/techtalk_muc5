<?php

namespace TechTalkTest;

use PHPUnit\Framework\TestCase;
use TechTalk\Item;
use TechTalk\Stack;

class StackMockTest extends TestCase
{
    public function testC()
    {
        // Create a stub for the class
        $item = $this->createMock(Item::class);

        // Configure the stub
        $item->method('getIdentifier')
            ->willReturn('id123');

        /** @var Item $item */
        $this->assertEquals('id123', $item->getIdentifier());

        $stack = new Stack();
        $stack->addItem($item);

        $this->assertEquals(1, $stack->countItems());
    }
}
