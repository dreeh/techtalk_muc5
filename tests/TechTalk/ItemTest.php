<?php

namespace TechTalkTest;

use PHPUnit\Framework\TestCase;
use TechTalk\Item;

//important:
//1. Class name postfix Test
//2. Class extends PHPUnit_Framework_TestCase
//3. Methods prefix test and public

class ItemTest extends TestCase
{
    public function testGetIdentifier()
    {
        $item = new Item('id1');

        //assertions are starting with assert
        //see for assertions: https://phpunit.de/manual/current/en/appendixes.assertions.html

        $this->assertInstanceOf(Item::class, $item);
        $this->assertEquals('id1', $item->getIdentifier());
    }
}
