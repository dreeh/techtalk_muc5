<?php

namespace TechTalkTest;

use PHPUnit\Framework\TestCase;
use TechTalk\Item;
use TechTalk\Stack;

class StackAccessProtectedTest extends TestCase
{
    public function testGetStack()
    {
        $method = new \ReflectionMethod(Stack::class, 'getStack');
        $method->setAccessible(true);

        $stack = new Stack();
        $stack->addItem(new Item('id123'));

        /** @var Item[] $items */
        $items = $method->invoke($stack);

        $this->assertEquals(['id123' => new Item('id123')], $items);
    }
}
