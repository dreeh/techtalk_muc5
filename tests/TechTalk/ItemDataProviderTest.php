<?php

namespace TechTalkTest;

use PHPUnit\Framework\TestCase;
use TechTalk\Item;

class ItemDataProviderTest extends TestCase
{
    /**
     * @param $identifier
     * @param $expectedTransformedIdentifier
     *
     * @dataProvider testGetIdentifierDataProvider
     */
    public function testGetIdentifierWithDataProvider($identifier, $expectedTransformedIdentifier)
    {
        $item = new Item($identifier);

        $this->assertEquals($expectedTransformedIdentifier, $item->getIdentifier());
    }

    public function testGetIdentifierDataProvider()
    {
        return [
            ['id-without-space', 'id-without-space'],
            ['id-with-space-right ', 'id-with-space-right'],
            [' id-with-space-left', 'id-with-space-left'],
        ];
    }
}
