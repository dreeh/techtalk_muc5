<?php

namespace TechTalk;

class Stack
{
    protected $myStack = [];

    /**
     * Stack constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return array
     */
    protected function getStack()
    {
        return $this->myStack;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->myStack[$item->getIdentifier()] = $item;
    }

    /**
     * @return int
     */
    public function countItems()
    {
        return count($this->myStack);
    }

    /**
     * untested method
     */
    public function shuffleItems()
    {
        shuffle($this->myStack);
    }
}
