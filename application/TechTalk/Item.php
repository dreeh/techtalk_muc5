<?php

namespace TechTalk;

class Item
{
    protected $identifier;

    /**
     * Item constructor.
     * @param string $identifier
     */
    public function __construct($identifier)
    {
        $this->identifier = trim($identifier);
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
}
