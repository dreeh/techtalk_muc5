<?php

namespace TechTalk;

class BadCrapValue
{
    /**
     * Body (text)
     *
     * @var string
     */
    protected $_body = NULL;

    /**
     * Body (html)
     *
     * @var string
     */
    protected $_htmlbody = NULL;

    /**
     * Attachments
     *
     * Keys: "content-type", "filename", "data"
     *
     * @var array
     */
    public $_attachments = array();

    /**
     * Inlines
     *
     * Keys: "content-type", "content-id", "filename", "data"
     *
     * @var array
     */
    public $_inlines = array();

    /**
     * Header
     *
     * @var array of string => string
     */
    protected $_headers = array();

    /**
     * Parses header data
     *
     * @param string $source Header
     * @return array of string => string
     */
    public function parse_headers(&$source)
    {

        if ($source != '') {

            $source = preg_replace('/\r?\n/', "\r\n", $source);
            $source = preg_replace("/\r\n(\t| )+/", ' ', $source);
            $headers = explode("\r\n", trim($source));

            foreach ($headers AS $line) {

                $name = strtolower(substr($line, 0, $pos = strpos($line, ':')));
                $value = substr($line, $pos + 1);

                if ($value[0] == ' ')
                    $value = substr($value, 1);

                if (isset($result[$name]) && is_array($result[$name])) {
                    $result[$name][] = $this->_decode_header_value($value);
                } else {
                    $result[$name] = array($this->_decode_header_value($value));
                }

            }

        } else {
            $result = array();
        }

        return $result;

    }

    /**
     * Decodiert die Value Teil des Name/Value Headerpaars.
     *
     * @param string $line Value Teil
     * @return string
     */
    protected function _decode_header_value($line)
    {

        // white spaces zwischen den Wörtern rausnehmen
        $line = preg_replace('/(=\?[^?]+\?(q|b)\?[^?]*\?=)(\s)+=\?/i', '\1=?', $line);

        while (preg_match('/(=\?([^?]+)\?(q|b)\?([^?]*)\?=)/i', $line, $matches)) {

            $encoded = $matches[1];
            $charset = $matches[2];
            $encoding = $matches[3];
            $text = $matches[4];

            switch (strtolower($encoding)) {

                case 'b':

                    $text = base64_decode($text);
                    break;

                case 'q':

                    $text = preg_replace_callback('/=([a-f0-9]{2})/i', function ($matches) {
                        return chr(hexdec($matches[1]));
                    }, str_replace('_', ' ', $text));
                    break;

            }

            if (strtolower($charset) != 'utf-8') {
                $text = @iconv($charset . '//IGNORE', 'UTF-8', $text);
            }

            $line = str_replace($encoded, $text, $line);
        }

        return $line;

    }

    /**
     * Splittet den Value Teil des Name/Value Headerpaars weiter auf;
     * z. B. bei Content-Type: text/plain; charset="ISO-8859-15"
     * in value="text/plain" und parameter=array('charset' => 'ISO-8859-15')
     *
     * @param string $line Value Teil
     */
    protected function _parse_header_value($line)
    {

        $return['value'] = trim($line);
        $return['parameter'] = array();

        if (($pos = strpos($line, ';')) !== false) {

            $return['value'] = trim(substr($line, 0, $pos));
            $line = trim(substr($line, $pos+1));

            if (strlen($line) > 0) {

                $parameters = preg_split('/\s*(?<!\\\\);\s*/i', $line);

                for ($i = 0; $i < count($parameters); $i++) {

                    $name  = substr($parameters[$i], 0, $pos = strpos($parameters[$i], '='));
                    $value = substr($parameters[$i], $pos + 1);
                    if ($value[0] == '"') {
                        $value = stripslashes(substr($value, 1, -1));
                    }
                    $return['parameter'][$name] = $value;
                    $return['parameter'][strtolower($name)] = $value;

                }

            }

        }

        return $return;

    }

    /**
     * Splittet die Daten in Header und Body auf
     *
     * @param string $source Daten
     */
    protected function _split_header_body(&$source)
    {

        if (preg_match("/^(.*?)\r?\n\r?\n(.*)/s", $source, $matches)) {
            return array($matches[1], $matches[2]);
        }

        return false;

    }

    /**
     * Splittet den Body in die einzelnen Parts auf
     *
     * @param string $source Daten
     * @param string $boundary Boundary
     */
    protected function _split_boundary(&$source, $boundary)
    {

        $tmp = preg_split('/--' . preg_quote($boundary, '/') . "(--)?\r?\n/", $source);

        $parts = array();

        for ($i = 1; $i < count($tmp)-1; $i++) {
            $parts[] = $tmp[$i];
        }

        return $parts;

    }

    /**
     * Dekodiert die Eingabe mittels UUE
     *
     * @param string $source UUE encoded Eingabe
     * @return string dekodiertes Ergebnis
     */
    protected function _uue_decode(&$source)
    {

        $file = '';
        $str = preg_split("/\r?\n/", trim($source));
        $strlen = count($str);

        for ($i = 0; $i < $strlen; $i++) {
            $pos = 1;
            $d = 0;
            $len=(int)(((ord(substr($str[$i], 0, 1)) -32) - ' ') & 077);

            while (($d + 3 <= $len) AND ($pos + 4 <= strlen($str[$i]))) {
                $c0 = (ord(substr($str[$i], $pos, 1)) ^ 0x20);
                $c1 = (ord(substr($str[$i], $pos+1, 1)) ^ 0x20);
                $c2 = (ord(substr($str[$i], $pos+2, 1)) ^ 0x20);
                $c3 = (ord(substr($str[$i], $pos+3, 1)) ^ 0x20);
                $file .= chr(((($c0 - ' ') & 077) << 2) | ((($c1 - ' ') & 077) >> 4));

                $file .= chr(((($c1 - ' ') & 077) << 4) | ((($c2 - ' ') & 077) >> 2));

                $file .= chr(((($c2 - ' ') & 077) << 6) |  (($c3 - ' ') & 077));

                $pos += 4;
                $d += 3;
            }

            if (($d + 2 <= $len) && ($pos + 3 <= strlen($str[$i]))) {
                $c0 = (ord(substr($str[$i], $pos, 1)) ^ 0x20);
                $c1 = (ord(substr($str[$i], $pos+1, 1)) ^ 0x20);
                $c2 = (ord(substr($str[$i], $pos+2, 1)) ^ 0x20);
                $file .= chr(((($c0 - ' ') & 077) << 2) | ((($c1 - ' ') & 077) >> 4));

                $file .= chr(((($c1 - ' ') & 077) << 4) | ((($c2 - ' ') & 077) >> 2));

                $pos += 3;
                $d += 2;
            }

            if (($d + 1 <= $len) && ($pos + 2 <= strlen($str[$i]))) {
                $c0 = (ord(substr($str[$i], $pos, 1)) ^ 0x20);
                $c1 = (ord(substr($str[$i], $pos+1, 1)) ^ 0x20);
                $file .= chr(((($c0 - ' ') & 077) << 2) | ((($c1 - ' ') & 077) >> 4));

            }
        }

        return $file;

    }

    /**
     * Dekodiert den Body mittels der angegebenen Methode
     *
     * @param string $body Body
     * @param string $encoding Encoding
     * @param string $charset Charset
     * @param string $destination_charset Destination charset
     */
    protected function _decode_body(&$body, $encoding, $charset = 'iso-8859-1', $destination_charset = null)
    {

        // !TODO make positive list of supported charsets
        // and check against them
        if (strtolower($charset) == 'x-unknown' || strtolower($charset) == 'us-ascii') {
            $charset = 'iso-8859-15';
        }

        $convert_to_destination_charset = !empty($charset) && !empty($destination_charset) && strtolower($charset) != strtolower($destination_charset);

        switch ($encoding) {

            case 'quoted-printable':
                if ($convert_to_destination_charset) {
                    return @iconv($charset . '//IGNORE', $destination_charset, preg_replace_callback('/=([a-f0-9]{2})/i', function ($matches) {return chr(hexdec($matches[1]));}, preg_replace("/=\r?\n/", '', $body)));
                } else {
                    return preg_replace_callback('/=([a-f0-9]{2})/i', function ($matches) {return chr(hexdec($matches[1]));}, preg_replace("/=\r?\n/", '', $body));
                }

                break;

            case 'base64':
                if ($convert_to_destination_charset) {
                    return @iconv($charset . '//IGNORE', $destination_charset, base64_decode($body));
                } else {
                    return base64_decode($body);
                }

                break;

            case 'x-uue':
                if ($convert_to_destination_charset) {
                    return @iconv($charset . '//IGNORE', $destination_charset, $this->_uue_decode($body));
                } else {
                    return $this->_uue_decode($body);
                }

                break;

            case '7bit':
            case '8bit':
            default:
                if ($convert_to_destination_charset) {
                    return @iconv($charset . '//IGNORE', $destination_charset, $body);
                } else {
                    return $body;
                }

        }

    }

    /**
     * Parst die Mail und speichert Body, HTML-Body, Attachments sowie Inline-Attachments
     * Für jeden Unterteil wird diese Funktion rekursiv aufgerufen
     *
     * @param string $header Header
     * @param string $body Body
     * @param string $contenttype Content-Type des Bodys
     */
    protected function _parse(&$header, &$body, $contenttype = 'text/plain')
    {
        set_time_limit(60);

        $headers = $this->parse_headers($header);

        if ($this->_headers === NULL) {
            $this->_headers = $headers;
        }

        if (!isset($headers['content-type'])) {
            $headers['content-type'][0] = $contenttype;
        }

        $tmp = $this->_parse_header_value($headers['content-type'][0]);
        $contenttype = $tmp['value'];
        $contenttype_parameter = $tmp['parameter'];

        if (preg_match('/([0-9a-z+.-]+)\/([0-9a-z+.-]+)/i', $contenttype, $matches)) {
            $contenttype_primary = $matches[1];
            $contenttype_secondary = $matches[2];
        }

        $contenttype_parameter_charset = (isset($contenttype_parameter['charset']) ? strtolower($contenttype_parameter['charset']) : null);
        $contenttransferencoding = (isset($headers['content-transfer-encoding'][0]) ? strtolower($headers['content-transfer-encoding'][0]) : '7bit');

        $filename = !empty($contenttype_parameter['name']) ? $contenttype_parameter['name'] : '';

        if (isset($headers['content-disposition'][0]) && $headers['content-disposition'][0] != '') {

            $tmp = $this->_parse_header_value($headers['content-disposition'][0]);
            $contentdisposition = strtolower($tmp['value']);
            $contentdisposition_parameter = $tmp['parameter'];

            if (!empty($contentdisposition_parameter['filename'])) {
                $filename = $contentdisposition_parameter['filename'];
            }

            if ($contentdisposition == 'attachment') {

                $this->_attachments[] = array(
                    'content-type' => $contenttype,
                    'filename' => $filename,
                    'data' => $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset)
                );

                return;

            } else if ($contentdisposition == 'inline') {

                $contentid = (isset($headers['content-id'][0]) ? $headers['content-id'][0] : '');
                $contentid = preg_replace('/<?(.*?:)?(.+?)>?/', '\\2', $contentid);

                $contentlocation = (isset($headers['content-location'][0]) ? $headers['content-location'][0] : '');

                if (!empty($contentid) || !empty($contentlocation)) {

                    // Inlines must have either a content-id or a content-location header set
                    // so that they can referenced within another part code (i.e. text/html)

                    $this->_inlines[] = array(
                        'content-type' => $contenttype,
                        'content-id' => $contentid,
                        'content-location' => $contentlocation,
                        'filename' => $filename,
                        'data' => $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset)
                    );

                    return;

                }

            }

        } elseif ((isset($headers['content-id'][0])) && !($this->_body === NULL && $contenttype == 'text/plain')) {

            // inline without Content-Dispositon (certainly not RFC compliant) but with Content-Id

            $contentid = (isset($headers['content-id'][0]) ? $headers['content-id'][0] : '');
            $contentid = preg_replace('/<?(.*?:)?(.+?)>?/', '\\2', $contentid);

            $contentlocation = (isset($headers['content-location'][0]) ? $headers['content-location'][0] : '');

            $this->_inlines[] = array(
                'content-type' => $contenttype,
                'content-id' => $contentid,
                'content-location' => $contentlocation,
                'filename' => $filename,
                'data' => $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset)
            );

            return;

        }


        switch (strtolower($contenttype)) {

            case 'text/plain':

                if ($this->_body === NULL) {

                    $this->_body = $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset, 'utf-8');

                    if (preg_match_all("/begin ([0-7]{3}) (.+)\r?\n(.+)\r?\nend/Us", $this->_body, $matches)) {

                        for ($i = 0; $i < count($matches[3]); $i++) {
                            $this->_attachments[] = array('content-type' => 'application/octet-stream', 'filename' => $matches[2][$i], 'data' => $this->_uue_decode($matches[3][$i]));
                        }

                        $this->_body = preg_replace("/begin ([0-7]{3}) (.+)\r?\n(.+)\r?\nend/Us", '', $this->_body);

                    }

                } else {
                    $this->_attachments[] = array('content-type' => $contenttype, 'filename' => $filename, 'data' => $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset));
                }

                break;

            case 'text/html':

                if ($this->_htmlbody === NULL) {
                    $this->_htmlbody = $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset, 'utf-8');
                } else {
                    $this->_attachments[] = array('content-type' => $contenttype, 'filename' => $filename, 'data' => $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset));
                }

                break;

            case 'multipart/parallel':
            case 'multipart/report': // RFC1892
            case 'multipart/signed': // PGP
            case 'multipart/digest':
            case 'multipart/alternative':
            case 'multipart/related':
            case 'multipart/mixed':
            case 'multipart/appledouble':

                if (!isset($contenttype_parameter['boundary'])) {
                    return false;
                }

                $default_ctype = (strtolower($contenttype) === 'multipart/digest') ? 'message/rfc822' : 'text/plain';

                $parts = $this->_split_boundary($body, $contenttype_parameter['boundary']);

                for ($i = 0; $i < count($parts); $i++) {
                    list($part_header, $part_body) = $this->_split_header_body($parts[$i]);
                    $part = $this->_parse($part_header, $part_body, $default_ctype);
                }

                break;

            case 'message/rfc822':
                // die('message/rfc822 without content-disposition');
                // break;

            default:
                $this->_attachments[] = array('content-type' => $contenttype, 'filename' => $filename, 'data' => $this->_decode_body($body, $contenttransferencoding, $contenttype_parameter_charset));
                break;

        }

    }

    /**
     * Dekodiert eine E-Mail Adresse aus den Feldern To, From, CC oder BCC
     * in ein Array mit den Feldern "name" und "address"
     *
     * @param string $address E-Mail Adresse
     */
    protected function _decode_mail_address($address)
    {

        if ($address != '') {

            if (preg_match('/(.*?)\s*<(.*?)>/', $address, $matches)) {

                $matches[1] = trim($matches[1]);
                if (isset($matches[1]{0}) && $matches[1]{0} == '"') {
                    $matches[1] = substr($matches[1], 1, -1);
                }
                return array('name' => $matches[1], 'address' => $matches[2]);

            } elseif (preg_match('/(.*?)\s*\((.*?)\)/', $address, $matches)) {

                $matches[2] = trim($matches[2]);
                if (isset($matches[2]{0}) && $matches[2]{0} == '"') {
                    $matches[2] = substr($matches[2], 1, -1);
                }
                return array('name' => $matches[2], 'address' => $matches[1]);

            } else {
                return array('name' => '', 'address' => $address);
            }

        } else {
            return false;
        }

    }

    /**
     * Parst die Mail und speichert Body, HTML-Body, Attachments sowie Inline-Attachments
     *
     * @param string $source Komplette E-Mail mit Headern und Body
     */
    public function parse(&$source)
    {

        if ($source == '') {
            throw new Exception('Mimedecode::parse() - error in parameter: $source may not be empty');
        }

        $this->_body = NULL;
        $this->_htmlbody = NULL;
        $this->_attachments = array();
        $this->_inlines = array();
        $this->_headers = NULL;

        $res = $this->_split_header_body($source);

        if ($res === false) {
            throw new Exception('Mimedecode::parse() - Unable to split message in header and body parts');
        }

        $this->_parse($res[0], $res[1]);

    }

    /**
     * Liefert das Subject zurück
     *
     * @return string Subject
     */
    public function get_subject()
    {

        if (isset($this->_headers['subject'][0])) {
            return $this->_headers['subject'][0];
        } else {
            return false;
        }

    }

    /**
     * Liefert die From E-Mail Adresse als Array mit den Feldern "name" und "address" zurück
     *
     * @return array E-Mail Adresse in Arraystruktur
     */
    public function get_from()
    {

        if (isset($this->_headers['from'][0])) {
            return $this->_decode_mail_address($this->_headers['from'][0]);
        } else {
            return false;
        }

    }

    /**
     * Liefert die From E-Mail Adresse als String zurück
     *
     * @return string E-Mail Adresse als String
     */
    public function get_from_str()
    {

        $from_str = '';

        if (($from = $this->get_from()) !== false) {

            if ($from['name'] != '') {
                $from_str = (strpos($from['name'], ',') === false ? $from['name'] : '"' . $from['name'] . '"') . ' <' . $from['address'] . '>';
            } else {
                $from_str = $from['address'];
            }

        }

        return $from_str;

    }

    /**
     * Liefert die To E-Mail Adresse(n) als Array of Array mit den Feldern "name" und "address" zurück
     *
     * @return array Array of E-Mail Adresse in Arraystruktur
     */
    public function get_to()
    {

        if (isset($this->_headers['to'][0])) {

            $tmp = explode(',', preg_replace('/(\(.*?),(.*?\))/', "\\1\xFF\\2", preg_replace('/(".*?),(.*?")/', "\\1\xFF\\2", $this->_headers['to'][0])));
            $result = array();

            for ($i = 0; $i < count($tmp); $i++) {

                $tmp[$i] = trim(str_replace("\xFF", ',', $tmp[$i]));
                if ($tmp[$i] != '') {
                    $result[] = $this->_decode_mail_address($tmp[$i]);
                }

            }

            if (count($result) > 0) {
                return $result;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    /**
     * Liefert die To E-Mail Adresse(n) als String zurück
     *
     * @return string To E-Mail Adresse(n) als String
     */
    public function get_to_str()
    {

        $to_str = '';

        if (($to_array = $this->get_to(false)) !== false) {

            foreach ($to_array AS $key => $to) {

                if ($to['name'] != '') {
                    $to_str .= (strpos($to['name'], ',') === false ? $to['name'] : '"' . $to['name'] . '"') . ' <' . $to['address'] . '>, ';
                } else {
                    $to_str .= $to['address'] . ', ';
                }

            }

        }

        return substr($to_str, 0, -2);

    }

    /**
     * Liefert die CC E-Mail Adresse(n) als Array of Array mit den Feldern "name" und "address" zurück
     *
     * @return array Array of E-Mail Adresse in Arraystruktur
     */
    public function get_cc()
    {

        if (isset($this->_headers['cc'][0])) {

            $tmp = explode(',', preg_replace('/(\(.*?),(.*?\))/', "\\1\xFF\\2", preg_replace('/(".*?),(.*?")/', "\\1\xFF\\2", $this->_headers['cc'][0])));
            $result = array();

            for ($i = 0; $i < count($tmp); $i++) {

                $tmp[$i] = trim(str_replace("\xFF", ',', $tmp[$i]));
                if ($tmp[$i] != '') {
                    $result[] = $this->_decode_mail_address($tmp[$i]);
                }

            }

            if (count($result) > 0) {
                return $result;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    /**
     * Liefert die CC E-Mail Adresse(n) als String zurück
     *
     * @return string CC E-Mail Adresse(n) als String
     */
    public function get_cc_str()
    {

        $cc_str = '';

        if (($cc_array = $this->get_cc()) !== false) {

            foreach ($cc_array AS $key => $cc) {

                if ($cc['name'] != '') {
                    $cc_str .= (strpos($cc['name'], ',') === false ? $cc['name'] : '"' . $cc['name'] . '"') . ' <' . $cc['address'] . '>, ';
                } else {
                    $cc_str .= $cc['address'] . ', ';
                }

            }

        }

        return substr($cc_str, 0, -2);

    }

    /**
     * Liefert die Reply-To E-Mail Adresse als Array mit den Feldern "name" und "address" zurück
     *
     * @return array E-Mail Adresse in Arraystruktur
     */
    public function get_replyto()
    {

        if (isset($this->_headers['reply-to'][0])) {
            return $this->_decode_mail_address($this->_headers['reply-to'][0]);
        } else {
            return false;
        }

    }

    /**
     * Liefert die Reply-To E-Mail Adresse als String zurück
     *
     * @return string E-Mail Adresse als String
     */
    public function get_replyto_str()
    {

        $replyto_str = '';

        if (($replyto = $this->get_replyto()) !== false) {

            if ($replyto['name'] != '') {
                $replyto_str .= (strpos($replyto['name'], ',') === false ? $replyto['name'] : '"' . $replyto['name'] . '"') . ' <' . $replyto['address'] . '>';
            } else {
                $replyto_str = $replyto['address'];
            }

        }

        return $replyto_str;

    }

    /**
     * Liefert das Date Feld zurück
     *
     * @return string Date Feld
     */
    public function get_date()
    {

        if (isset($this->_headers['date'][0])) {
            return date('r', strtotime(preg_replace('/[^a-zA-Z0-9\,\+\-\.\:\s]/i', '', $this->_headers['date'][0])));
            //return $this->_headers['date'][0];
        } else {
            return false;
        }

    }

    /**
     * Liefert das Date Feld als Unix Timestamp zurück
     *
     * @return integer Unix Timestamp
     */
    public function get_date_unixtime()
    {

        if (isset($this->_headers['date'][0])) {
            return strtotime(preg_replace('/[^a-zA-Z0-9\,\+\-\.\:\s]/i', '', $this->_headers['date'][0]));
        } else {
            return false;
        }

    }

    /**
     * Liefert das Date Feld als ISO 8601 Datum mit Uhrzeit zurück
     *
     * @return integer ISO 8601 Datum mit Uhrzeit
     */
    public function get_date_iso()
    {

        if (isset($this->_headers['date'][0])) {
            return date('Y-m-d H:i:s', strtotime(preg_replace('/[^a-zA-Z0-9\,\+\-\.\:\s]/i', '', $this->_headers['date'][0])));
        } else {
            return false;
        }

    }

    /**
     * Liefert den Mailer zurück
     *
     * @return string Mailer
     */
    public function get_mailer()
    {

        if (isset($this->_headers['x-mailer'][0])) {
            return $this->_headers['x-mailer'][0];
        } else if (isset($this->_headers['user-agent'][0])) {
            return $this->_headers['user-agent'][0];
        } else {
            return false;
        }

    }

    /**
     * Liefert die Priorität zurück
     *
     * @return integer Priorität
     */
    public function get_priority()
    {

        if (isset($this->_headers['x-priority'][0])) {

            if (preg_match('/(\d+)/', $this->_headers['x-priority'][0], $matches)) {
                return $matches[1];
            } else {
                return 3;
            }

        } else {
            return 3;
        }

    }

    /**
     * Liefert den Text-Body zurück
     *
     * @return string Text-Body
     */
    public function get_body()
    {
        return $this->_body;
    }

    /**
     * Liefert den HTML-Body zurück
     *
     * @return string HTML-Body
     */
    public function get_htmlbody()
    {
        return $this->_htmlbody;
    }

    /**
     * Returns array of with all attachments
     *
     * @return array
     */
    public function get_attachments()
    {
        return $this->_attachments;
    }

    /**
     * Returns an attachment identified by its name
     *
     * @param string $filename
     * @return array
     */
    public function get_attachment_by_filename($filename)
    {

        check_string($filename, 'filename');

        if (!is_array($this->_attachments)) {
            return null;
        }

        foreach ($this->_attachments as $attachment) {

            if (isset($attachment['filename']) && $attachment['filename'] == $filename) {
                return $attachment;
            }

        }

        return null;

    }

    /**
     * Returns headers as string
     *
     * @return string
     */
    public function get_header_str()
    {

        $str = '';
        foreach ($this->_headers as $key => $value) {

            if (!is_array($value)) {
                $str .= $key . ': ' . (string)$value . "\r\n";
            } else {

                foreach ($value as $key2 => $value2) {
                    $str .= $key . ': ' . (string)$value2 . "\r\n";
                }
            }
        }

        return $str;

    }

}
