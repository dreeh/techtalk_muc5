# Tech Talk Muc 5

## 26. Juli 2016, Denny Reeh

    https://bitbucket.org/dreeh/techtalk_muc5

## Unittests

### Agenda

#### Einfaches Projektsetup

#### Testen

#### Test Driven Development

#### Code Coverage

    phpunit --coverage-html /home/denny.reeh/projects/sandbox/public/coverage

    http://sandbox.denny.c24/coverage/index.html
